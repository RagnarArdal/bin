My Shell Scripts
================

This repository is for all of my shell scripts.

Unless stated otherwise, the scripts are written by me.

st -- SageTex TypeSetter
------------------------

A script that compiles LaTeX files that use the SageTex package.
It takes care of all the extra steps needed for the document to be typeset correctly.
It also cleans up afterwards so the only file it produces is the PDF file.
